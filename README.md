# An iot clock

Running on an ESP8266, this is an iot clock that accepts MQTT commands.

A weekend project, which does not have documentation or much of anything.
If I were to do it again, I'd probably use [Sonoff-Tasmota](https://github.com/arendst/Sonoff-Tasmota).

![clock pic](clock.jpg)