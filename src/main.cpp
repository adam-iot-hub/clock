#include <Arduino.h>

#include <NTPClient.h>

#include <Time.h>
#include <TimeLib.h>
#include <Timezone.h>

#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

#include <PubSubClient.h>

#include "config.h"
#include "Adafruit_NeoPixel.h"

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "pool.ntp.org", 0, 6 * 60 * 60 * 1000); // 6 hours ntp update interval

WiFiClient wifi_client;

IPAddress MQTTserver(192, 168, 35, 250);
// IPAddress MQTTserver(192, 168,33,75);
PubSubClient client(wifi_client);

String date;
String t;
String tempC;
const char *days[] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
const char *months[] = {"Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec"};
const char *ampm[] = {"AM", "PM"};

// Parameter 1 = number of pixels in strip
// Parameter 2 = pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
#define NUM_LEDS 12
#define LED_OFFSET 3
Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LEDS, 14, NEO_GRB + NEO_KHZ400);
int led_brightness = 255;

void mqtt_callback(char *topic, byte *payload, unsigned int length)
{
    payload[length] = '\0';
    String s = String((char*)payload);
    String t = String(topic);


    Serial.print("Message arrived [");
    Serial.print(topic);
    Serial.print("] ");
    Serial.println(s);



    if(t == "ADAM-IOT-CLOCK/brightness"){
        int led_brightness = s.toInt();
        Serial.println("Setting brighness to " + String(led_brightness));
        strip.setBrightness(led_brightness);
    }
}

void mqtt_connect()
{
    while (!client.connected())
    {
        Serial.print("Attempting MQTT connection...");
        // Create a random client ID
        String clientId = "ADAM-IOT-CLOCK";
        // Attempt to connect
        if (client.connect(clientId.c_str()))
        {
            Serial.println("connected sending status/ hello world");
            // Once connected, publish an announcement...
            Serial.println(client.publish("ADAM-IOT-CLOCK/status", "hello world"));
            // ... and resubscribe
            Serial.println(client.subscribe("ADAM-IOT-CLOCK/brightness"));
        }
        else
        {
            Serial.print("failed, rc=");
            Serial.print(client.state());
            Serial.println(" try again in 5 seconds");
            // Wait 5 seconds before retrying
            delay(5000);
        }
    }
}

void setup()
{
    Serial.begin(115200);

    Serial.println("Beginning led strip");
    strip.begin();

    Serial.println("setting 255 255 255");
    for (int i = 0; i < NUM_LEDS; i++)
    {
        strip.setPixelColor(i, 255, 255, 255);
    }
    strip.show();

    WiFi.begin(ssid, password);
    while (WiFi.status() != WL_CONNECTED)
    {
        delay(500);
        Serial.print(".");
    }

    timeClient.begin();

    Serial.println("setting 0 0 0");
    for (int i = 0; i < NUM_LEDS; i++)
    {
        strip.setPixelColor(i, 0, 0, 0);
    }
    strip.show();

    Serial.print(WiFi.localIP());
    Serial.print(" on ");
    Serial.println(ssid);

    client.setServer(MQTTserver, 1883);
    client.setCallback(mqtt_callback);
}

time_t getLocalTime()
{
    timeClient.update();
    unsigned long epochTime = timeClient.getEpochTime();

    // convert received time stamp to time_t object
    time_t local, utc;
    utc = epochTime;

    // Then convert the UTC UNIX timestamp to local time
    // TimeChangeRule usPDT = {"PDT", Second, Sun, Mar, 2, -420};  //UTC - 7 hours
    // TimeChangeRule usPST = {"PST", First, Sun, Nov, 2, -480};   //UTC - 8 hours
    // Timezone usPacific(usPDT, usPST);
    // local = usPacific.toLocal(utc);

    TimeChangeRule usEDT = {"EDT", Second, Sun, Mar, 2, -240}; //UTC - 4 hours
    TimeChangeRule usEST = {"EST", First, Sun, Nov, 2, -300};  //UTC - 5 hours
    Timezone usEastern(usEDT, usEST);
    local = usEastern.toLocal(utc);

    return local;
}

void tellTime()
{
    date = ""; // clear the variables
    t = "";

    time_t local = getLocalTime();

    // now format the Time variables into strings with proper names for month, day etc
    date += days[weekday(local) - 1];
    date += ", ";
    date += months[month(local) - 1];
    date += " ";
    date += day(local);
    date += ", ";
    date += year(local);

    // format the time to 12-hour format with AM/PM and no seconds
    t += hourFormat12(local);
    t += ":";
    if (minute(local) < 10) // add a zero if minute is under 10
        t += "0";
    t += minute(local);
    t += " ";
    t += ampm[isPM(local)];

    // Display the date and time
    Serial.println("");
    Serial.print("Local date: ");
    Serial.print(date);
    Serial.println("");
        // strip.setBrightness(new_brightness);
    Serial.print("Local time: ");
    Serial.println(t);
}

void loop()
{
    delay(100);
    tellTime();

    mqtt_connect();
    client.loop();


    time_t local = getLocalTime();

    for (int i = 0; i < NUM_LEDS; i++)
    {
        strip.setPixelColor(i, 0, 0, 0);
    }

    if (hourFormat12(local) % 12 == (minute(local) / 5) % 12)
    {
        strip.setPixelColor((hourFormat12(local) + LED_OFFSET) % 12, 150, 150, 0);
    }
    else
    {
        strip.setPixelColor((hourFormat12(local) + LED_OFFSET) % 12, 150, 0, 0);
        strip.setPixelColor(((minute(local) / 5) + LED_OFFSET) % 12, 0, 150, 0);
    }

    if (second(local) % 2 == 0)
    {
        strip.setPixelColor(((second(local) / 5) + LED_OFFSET) % 12, 0, 0, 150);
    }

    strip.show();
}
